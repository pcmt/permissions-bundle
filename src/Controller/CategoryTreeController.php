<?php

declare(strict_types=1);

/**
 * Copyright (c) 2020, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

namespace PcmtPermissionsBundle\Controller;

use Akeneo\Pim\Enrichment\Bundle\Controller\Ui\CategoryTreeController as OriginalCategoryTreeController;
use Akeneo\Pim\Enrichment\Bundle\Doctrine\ORM\Counter\CategoryItemsCounterInterface;
use Akeneo\Pim\Enrichment\Component\Category\Form\CategoryFormViewNormalizerInterface;
use Akeneo\Pim\Enrichment\Component\Category\Query\CountTreesChildrenInterface;
use Akeneo\Tool\Component\Classification\Model\CategoryInterface;
use Akeneo\Tool\Component\Classification\Repository\CategoryRepositoryInterface;
use Akeneo\Tool\Component\StorageUtils\Factory\SimpleFactoryInterface;
use Akeneo\Tool\Component\StorageUtils\Remover\RemoverInterface;
use Akeneo\Tool\Component\StorageUtils\Saver\SaverInterface;
use Akeneo\Tool\Component\StorageUtils\Updater\ObjectUpdaterInterface;
use Akeneo\UserManagement\Bundle\Context\UserContext;
use Akeneo\UserManagement\Bundle\Doctrine\ORM\Repository\GroupRepository;
use Oro\Bundle\SecurityBundle\SecurityFacade;
use PcmtPermissionsBundle\Service\CategoryPermissionsDefaultProvider;
use PcmtPermissionsBundle\Updater\CategoryChildrenPermissionsUpdater;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CategoryTreeController extends OriginalCategoryTreeController
{
    /**
     * @var CategoryChildrenPermissionsUpdater
     */
    private $categoryChildrenPermissionsUpdater;

    /**
     * @var CategoryPermissionsDefaultProvider
     */
    private $categoryPermissionsDefaultProvider;

    private ObjectUpdaterInterface $categoryUpdater;

    private ValidatorInterface $validator;

    private NormalizerInterface $constraintViolationNormalizer;

    private NormalizerInterface $normalizer;

    private CategoryFormViewNormalizerInterface $categoryFormViewNormalizer;

    private GroupRepository $groupRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserContext $userContext,
        SaverInterface $categorySaver,
        RemoverInterface $categoryRemover,
        SimpleFactoryInterface $categoryFactory,
        CategoryRepositoryInterface $categoryRepository,
        SecurityFacade $securityFacade,
        TranslatorInterface $translator,
        NormalizerInterface $normalizer,
        ObjectUpdaterInterface $categoryUpdater,
        ValidatorInterface $validator,
        NormalizerInterface $constraintViolationNormalizer,
        CategoryItemsCounterInterface $categoryItemsCounter,
        CountTreesChildrenInterface $countTreesChildrenQuery,
        CategoryFormViewNormalizerInterface $categoryFormViewNormalizer,
        array $rawConfiguration,
        GroupRepository $groupRepository
    ) {
        parent::__construct(
            $eventDispatcher,
            $userContext,
            $categorySaver,
            $categoryRemover,
            $categoryFactory,
            $categoryRepository,
            $securityFacade,
            $translator,
            $normalizer,
            $categoryUpdater,
            $validator,
            $constraintViolationNormalizer,
            $categoryItemsCounter,
            $countTreesChildrenQuery,
            $categoryFormViewNormalizer,
            $rawConfiguration
        );
        $this->normalizer = $normalizer;
        $this->categoryFormViewNormalizer = $categoryFormViewNormalizer;
        $this->categoryUpdater = $categoryUpdater;
        $this->validator = $validator;
        $this->groupRepository = $groupRepository;
    }

    public function setCategoryPermissionsDefaultProvider(CategoryPermissionsDefaultProvider $provider): void
    {
        $this->categoryPermissionsDefaultProvider = $provider;
    }

    public function editAction(Request $request, $id): JsonResponse
    {
        if ($this->securityFacade->isGranted($this->buildAclName('category_edit')) === false) {
            throw new AccessDeniedException();
        }

        $category = $this->findCategory($id);
        $responseStatus = Response::HTTP_OK;
        $form = $this->createForm($this->rawConfiguration['form_type'], $category, $this->getFormOptions($category));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = [];
                parse_str($request->getContent(), $data);

                $ownAccess = $this->prepareGroups($this->checkAllOptionMixedIn($data['pim_category']['ownAccess']));
                $editAccess = $this->prepareGroups($this->checkAllOptionMixedIn($data['pim_category']['editAccess']));
                $viewAccess = $this->prepareGroups($this->checkAllOptionMixedIn($data['pim_category']['viewAccess']));

                $category->clearAccesses();
                $category->setOwnAccess($ownAccess);
                $category->setEditAccess($editAccess);
                $category->setViewAccess($viewAccess);

                $this->categorySaver->save($category);
                $applyOnChildren = $form->get('applyOnChildren')
                    ->getData();
                if ($applyOnChildren) {
                    $this->categoryChildrenPermissionsUpdater->update($category);
                }
            } else {
                $responseStatus = Response::HTTP_BAD_REQUEST;
            }
        }

        $rootCategory = null;
        if ($category->isRoot() === false) {
            $rootCategory = $this->findCategory($category->getRoot());
        }

        $normalizedCategory = $this->normalizer->normalize($category, 'internal_api');
        $normalizedCategory = array_merge($normalizedCategory, [
            'root' => $rootCategory === null ? null : $this->normalizer->normalize($rootCategory, 'internal_api'),
        ]);

        $formData = $this->formatFormView($form->createView());

        return new JsonResponse([
            'category' => $normalizedCategory,
            'form' => $formData,
        ], $responseStatus);
    }

    public function createAction(Request $request): JsonResponse
    {
        if ($this->securityFacade->isGranted($this->buildAclName('category_create')) === false) {
            throw new AccessDeniedException();
        }

        $category = $this->categoryFactory->create();
        $data = json_decode($request->getContent(), true);
        $this->categoryUpdater->update($category, $data);
        $violations = $this->validator->validate($category);
        $this->categoryPermissionsDefaultProvider->fill($category);

        $normalizedViolations = [];
        foreach ($violations as $violation) {
            $normalizedViolation = $this->constraintViolationNormalizer->normalize(
                $violation,
                'internal_api',
                [
                    'category' => $category,
                ]
            );
            $normalizedViolations[$normalizedViolation['path']] = $normalizedViolation['message'];
        }

        if (count($normalizedViolations) > 0) {
            return new JsonResponse($normalizedViolations, Response::HTTP_BAD_REQUEST);
        }

        $this->categorySaver->save($category);

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

    public function setCategoryChildrenPermissionsUpdater(
        CategoryChildrenPermissionsUpdater $categoryChildrenPermissionsUpdater
    ): void {
        $this->categoryChildrenPermissionsUpdater = $categoryChildrenPermissionsUpdater;
    }

    protected function getFormOptions(CategoryInterface $category): array
    {
        return [
            'validation_groups' => ['Default', 'uiForm'],
        ];
    }

    private function formatFormView(FormView $formView): array
    {
        $formData = [
            'label' => [],
            'errors' => [],
        ];
        if (isset($formView->children['label'])) {
            foreach ($formView->children['label']->children as $locale => $labelForm) {
                $formData['label'][$locale] = [
                    'value' => $labelForm->vars['value'],
                    'fullName' => $labelForm->vars['full_name'],
                    'label' => $labelForm->vars['label'],
                ];
            }
        }
        if (isset($formView->children['_token'])) {
            $formData['_token'] = [
                'value' => $formView->children['_token']->vars['value'],
                'fullName' => $formView->children['_token']->vars['full_name'],
            ];
        }

        if (isset($formView->children['viewAccess'])) {
            $formData['permissions'] = [
                'view' => $this->formatPermissionField($formView->children['viewAccess']),
                'edit' => $this->formatPermissionField($formView->children['editAccess']),
                'own' => $this->formatPermissionField($formView->children['ownAccess']),
                'apply_on_children' => [
                    'value' => $formView->children['applyOnChildren']->vars['value'],
                    'fullName' => $formView->children['applyOnChildren']->vars['full_name'],
                ],
            ];
        }

        // No error mapping for now
        foreach ($formView->vars['errors'] as $error) {
            $formData['errors'][] = $error->getMessage();
        }
        $formData['errors'] = array_unique($formData['errors']);

        return $formData;
    }

    private function formatPermissionField(FormView $formView): array
    {
        $options = array_map(fn ($choice) => [
            'label' => $choice->label,
            'value' => $choice->value,
        ], $formView->vars['choices']);
        $allIncluded = false;

        /** check if All option is mixed in */
        if (count($options) > 1) {
            foreach ($options as $option) {
                if ($option['label'] === 'All' && in_array($option['value'], $formView->vars['value'], true)) {
                    $allIncluded = true;
                    break;
                }
            }
        }

        return [
            'value' => $allIncluded ? ['8'] : $formView->vars['value'],
            'fullName' => $formView->vars['full_name'],
            'choices' => $options,
        ];
    }

    private function prepareGroups(array $groups)
    {
        $userGroups = [];

        foreach ($groups as $group) {
            $group = $this->groupRepository->find($group);
            array_push($userGroups, $group);
        }

        return $userGroups;
    }

    /**
     * return access array checks if ALL is mixed in
     */
    private function checkAllOptionMixedIn(array $accesses)
    {
        if (count($accesses) > 1 && in_array('8', $accesses, true)) {
            return ['8'];
        }
        return $accesses;
    }
}
