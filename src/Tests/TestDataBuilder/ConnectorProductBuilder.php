<?php

declare(strict_types=1);

/**
 * Copyright (c) 2020, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

namespace PcmtPermissionsBundle\Tests\TestDataBuilder;

use Akeneo\Pim\Enrichment\Component\Product\Connector\ReadModel\ConnectorProduct;
use Akeneo\Pim\Enrichment\Component\Product\Model\PriceCollection;
use Akeneo\Pim\Enrichment\Component\Product\Model\ProductPrice;
use Akeneo\Pim\Enrichment\Component\Product\Model\ReadValueCollection;
use Akeneo\Pim\Enrichment\Component\Product\Value\OptionValue;
use Akeneo\Pim\Enrichment\Component\Product\Value\PriceCollectionValue;
use Akeneo\Pim\Enrichment\Component\Product\Value\ScalarValue;

class ConnectorProductBuilder
{
    /**
     * @var ConnectorProduct
     */
    private $product;

    public function __construct()
    {
        $this->product = new ConnectorProduct(
            1,
            'identifier',
            new \DateTimeImmutable(),
            new \DateTimeImmutable(),
            true,
            'familyCode',
            [],
            [],
            'parentProductModelCode',
            [],
            [],
            [],
            new ReadValueCollection([
                OptionValue::value('a_simple_select', 'optionA'),
                PriceCollectionValue::value('a_price', new PriceCollection([new ProductPrice(50, 'EUR')])),
                ScalarValue::value('a_yes_no', false),
                ScalarValue::value('a_number_float', '12.5000'),
                ScalarValue::scopableLocalizableValue(
                    'a_localized_and_scopable_text_area',
                    'my pink tshirt',
                    'ecommerce',
                    'en_US'
                ),
            ]),
            null,
            null
        );
    }

    public function build(): ConnectorProduct
    {
        return $this->product;
    }
}
