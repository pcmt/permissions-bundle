# PCMT Permissions Bundle for Akeneo Pim

## Key features

Permissions Bundle is a bundle for PCMT. 

## Development
### Running Test-Suits
The PcmtPermissionsBundle is covered with tests and every change and addition has also to be covered with unit tests. It uses PHPUnit.

To run the tests you have to change to this project's root directory and run the following commands in your console:

```
make unit
```

### Coding style
PcmtPermissionsBundle the coding style can be checked with Easy Coding Standard.

```
make ecs
```
